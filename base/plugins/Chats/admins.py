import asyncio
import time
from base import Null_Bot , ADMINS
from pyrogram import filters , Client
from pyrogram.types import ChatPermissions
from pyrogram.errors import (
    UsernameInvalid,
    ChatAdminRequired,
    PeerIdInvalid,
    UserIdInvalid
)

from base.helpers.admincheck import admin_check

# Mute permissions
mute_permission = ChatPermissions(
    can_send_messages=False,
    can_send_media_messages=False,
    can_send_stickers=False,
    can_send_animations=False,
    can_send_games=False,
    can_use_inline_bots=False,
    can_add_web_page_previews=False,
    can_send_polls=False,
    can_change_info=False,
    can_invite_users=True,
    can_pin_messages=False
)
# Unmute permissions
unmute_permissions = ChatPermissions(
    can_send_messages=True,
    can_send_media_messages=True,
    can_send_stickers=True,
    can_send_animations=True,
    can_send_games=True,
    can_use_inline_bots=True,
    can_add_web_page_previews=True,
    can_send_polls=True,
    can_change_info=False,
    can_invite_users=True,
    can_pin_messages=False
)

@Client.on_message(filters.regex('unpin') & filters.group)
async def unpin_message(client, message):
    chat_id = message.chat.id
    can_pin = await admin_check(message)
    if can_pin:
        try:
            await client.unpin_chat_message(
                chat_id
            )
        except UsernameInvalid:
            await message.reply("`invalid username`")
            return

        except PeerIdInvalid:
            await message.reply("`invalid username or userid`")
            return

        except UserIdInvalid:
            await message.reply("`invalid userid`")
            return

        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return

        except Exception as e:
            await message.reply("`Error!`\n"
                        f"**Log:** `{e}`"
                    )
            return
    else:
        await message.reply("`permission denied`")

@Client.on_message(filters.regex('pin') & filters.group)
async def pin_message(client, message):
    chat_id = message.chat.id
    can_pin = await admin_check(message)
    if can_pin:
        try:
            if message.reply_to_message:
                disable_notification = True
                if len(message.command) >= 2 and message.command[1] in ['alert', 'notify', 'loud']:
                    disable_notification = False
                await client.pin_chat_message(
                    chat_id,
                    message.reply_to_message.message_id,
                    disable_notification=disable_notification
                )
        except UsernameInvalid:
            await message.reply("`invalid username`")
            return

        except PeerIdInvalid:
            await message.reply("`invalid username or userid`")
            return

        except UserIdInvalid:
            await message.reply("`invalid userid`")
            return

        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return

        except Exception as e:
            await message.reply("`Error!`\n"
                        f"**Log:** `{e}`"
                    )
            return
    else:
        await message.reply("`permission denied`")

@Client.on_message(filters.regex('mute') & filters.group)
async def mute_hammer(client, message):
    chat_id = message.chat.id
    can_mute = await admin_check(message)
    if can_mute:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        try:
            get_mem = await client.get_chat_member(
                    chat_id,
                    user_id
                    )
            await client.restrict_chat_member(
                chat_id=message.chat.id,
                user_id=user_id,
                permissions=mute_permission
            )
            await message.reply(
                f"[{get_mem.user.first_name}](tg://user?id={get_mem.user.id}) **Muted Indefinitely**")
        except Exception as e:
            await message.reply("`Error!`\n"
                    f"**Log:** `{e}`"
                )
            return
    else:
        await message.reply("`permission denied`")

@Client.on_message(filters.regex('unmute') & filters.group)
async def unmute(client, message):
    chat_id = message.chat.id
    can_unmute = await admin_check(message)
    if can_unmute:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        try:
            get_mem = await client.get_chat_member(
                    chat_id,
                    user_id
                    )
            await client.restrict_chat_member(
                chat_id=message.chat.id,
                user_id=user_id,
                permissions=unmute_permissions
            )
            await message.reply(
                    f"[{get_mem.user.first_name}](tg://user?id={get_mem.user.id}) **Unmuted**"
                    )
        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return
        except Exception as e:
            await message.reply("`Error!`\n"
                    f"**Log:** `{e}`"
                )
            return

@Client.on_message(filters.regex('kick') & filters.group)
async def kick_user(client, message):
    chat_id = message.chat.id
    can_kick = await admin_check(message)
    if can_kick:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        try:
            get_mem = await client.get_chat_member(
                chat_id,
                user_id
                )
            await client.kick_chat_member(chat_id, get_mem.user.id, int(time.time() + 45))
            await message.reply(
                f"[{get_mem.user.first_name}](tg://user?id={get_mem.user.id}) **Kicked**"
                )
        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return
        except Exception as e:
            await message.reply("`Error!`\n"
                f"**Log:** `{e}`"
            )
            return
    else:
        await message.reply("`permission denied`")

@Client.on_message(filters.regex('ban') & filters.group)
async def ban_usr(client, message):
    chat_id = message.chat.id
    can_ban = await admin_check(message)
    if can_ban:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        if user_id:
            try:
                get_mem = await client.get_chat_member(chat_id, user_id)
                await client.kick_chat_member(chat_id, user_id)
                await message.reply(
                    f"[{get_mem.user.first_name}](tg://user?id={get_mem.user.id}) **Banned**\n")

            except UsernameInvalid:
                await message.reply("`invalid username`")
                return

            except PeerIdInvalid:
                await message.reply("`invalid username or userid`")
                return

            except UserIdInvalid:
                await message.reply("`invalid userid`")
                return

            except ChatAdminRequired:
                await message.reply("`permission denied`")
                return

            except Exception as e:
                await message.reply(f"**Log:** `{e}`")
                return

    else:
        await message.reply("`permission denied`")
        return

@Client.on_message(filters.regex('unban') & filters.group)
async def unban_usr(client, message):
    chat_id = message.chat.id
    can_unban = await admin_check(message)
    if can_unban:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        try:
            get_mem = await client.get_chat_member(
                chat_id,
                user_id
                )
            await client.unban_chat_member(chat_id, get_mem.user.id)
            await message.reply(
                f"[{get_mem.user.first_name}](tg://user?id={get_mem.user.id}) **Unbanned**\n"
                )
        except UsernameInvalid:
            await message.reply("`invalid username`")
            return

        except PeerIdInvalid:
            await message.reply("`invalid username or userid`")
            return

        except UserIdInvalid:
            await message.reply("`invalid userid`")
            return

        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return

        except Exception as e:
            await message.reply(f"**Log:** `{e}`")
            return

@Client.on_message(filters.regex('promote') & filters.group)
async def promote_usr(client, message):
    chat_id = message.chat.id
    can_promo = await admin_check(message)

    if can_promo:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        get_mem = await client.get_chat_member(
                    chat_id,
                    user_id
                    )

    if user_id:
        try:
            await client.promote_chat_member(chat_id, user_id,
                                            can_change_info=True,
                                            can_delete_messages=True,
                                            can_restrict_members=True,
                                            can_invite_users=True,
                                            can_pin_messages=True)
            await message.reply(f"**Promoted** [{get_mem.user.first_name}](tg://user?id={get_mem.user.id})")

        except UsernameInvalid:
            await message.reply("`invalid username`")
            return
        except PeerIdInvalid:
            await message.reply("`invalid username or userid`")
            return
        except UserIdInvalid:
            await message.reply("`invalid userid`")
            return

        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return
        except Exception as e:
            await message.reply(f"**Log:** `{e}`")
            return

@Client.on_message(filters.regex('demote') & filters.group)
async def demote_usr(client, message):
    chat_id = message.chat.id
    can_demote = await admin_check(message)

    if can_demote:
        try:
            if message.reply_to_message:
                user_id = message.reply_to_message.from_user.id
            else:
                usr = await client.get_users(message.command[1])
                user_id = usr.id
        except IndexError:
            await message.reply('not defined error')
            return
        try:
            get_mem = await client.get_chat_member(
                chat_id,
                user_id
                )
            await client.promote_chat_member(chat_id, get_mem.user.id,
                                            can_change_info=False,
                                            can_delete_messages=False,
                                            can_restrict_members=False,
                                            can_invite_users=False,
                                            can_pin_messages=False)
            await message.reply(f"**Demoted** [{get_mem.user.first_name}](tg://user?id={get_mem.user.id})")
        except ChatAdminRequired:
            await message.reply("`permission denied`")
            return

        except Exception as e:
            await message.reply(f"**Log:** `{e}`")
            return

@Client.on_message(filters.regex('link') & filters.group)
async def invite_link(client, message):
    can_invite = await admin_check(message)
    if can_invite:
        try:
            link = await client.export_chat_invite_link(message.chat.id)
            await message.reply(text=link)
        except Exception as e:
            print(e)
            await message.reply("`permission denied`")

@Client.on_message(filters.regex('perm') & filters.group)
async def view_perm(client, message):
    """view group permission."""
    v_perm = ""
    vmsg = ""
    vmedia = ""
    vstickers = ""
    vanimations = ""
    vgames = ""
    vinlinebots = ""
    vwebprev = ""
    vpolls = ""
    vinfo = ""
    vinvite = ""
    vpin = ""

    v_perm = await client.get_chat(message.chat.id)

    def convert_to_Bool(val: bool):
        if val:
            return "<code>True</code>"
        else:
            return "<code>False</code>"

    vmsg = convert_to_Bool(v_perm.permissions.can_send_messages)
    vmedia = convert_to_Bool(v_perm.permissions.can_send_media_messages)
    vstickers = convert_to_Bool(v_perm.permissions.can_send_stickers)
    vanimations = convert_to_Bool(v_perm.permissions.can_send_animations)
    vgames = convert_to_Bool(v_perm.permissions.can_send_games)
    vinlinebots = convert_to_Bool(v_perm.permissions.can_use_inline_bots)
    vwebprev = convert_to_Bool(v_perm.permissions.can_add_web_page_previews)
    vpolls = convert_to_Bool(v_perm.permissions.can_send_polls)
    vinfo = convert_to_Bool(v_perm.permissions.can_change_info)
    vinvite = convert_to_Bool(v_perm.permissions.can_invite_users)
    vpin = convert_to_Bool(v_perm.permissions.can_pin_messages)

    if v_perm is not None:
        try:
            permission_view_str = ""

            permission_view_str += "<b>Chat permissions:</b>\n"
            permission_view_str += f"<b>Send Messages:</b> {vmsg}\n"
            permission_view_str += f"<b>Send Media:</b> {vmedia}\n"
            permission_view_str += f"<b>Send Stickers:</b> {vstickers}\n"
            permission_view_str += f"<b>Send Animations:</b> {vanimations}\n"
            permission_view_str += f"<b>Can Play Games:</b> {vgames}\n"
            permission_view_str += f"<b>Can Use Inline Bots:</b> {vinlinebots}\n"
            permission_view_str += f"<b>Webpage Preview:</b> {vwebprev}\n"
            permission_view_str += f"<b>Send Polls:</b> {vpolls}\n"
            permission_view_str += f"<b>Change Info:</b> {vinfo}\n"
            permission_view_str += f"<b>Invite Users:</b> {vinvite}\n"
            permission_view_str += f"<b>Pin Messages:</b> {vpin}\n"
            await message.reply(permission_view_str)
        except Exception as e:
            await message.reply("`Error!`\n" f"**Log:** `{e}`")